using System.Xml;
using System.Text;
using System.Globalization;
using Zogg.RecalBox.GameList.Objects.Atomic;

//var dt = DateTime.ParseExact("19870101T000000", "yyyyMMdd_HHmmssFF__", null);

//PlatformList platforms = new PlatformList();

XmlDocument document = new XmlDocument();
string xmlData = String.Empty;

/*
xmlData = "<provider><System>Mame</System><software>Skraper</software><database>ScreenScraper.fr</database><web>http://www.screenscraper.fr</web></provider>";
document.LoadXml(xmlData);
Provider provider = new Provider();
provider.FromXmlDocument(document);
*/

/*
xmlData = "<game id=\"13937\" source=\"ScreenScraper.fr\"><path>./3countb.zip</path><name>3 Count Bout (NGM-043 ~ NGH-043)</name><desc>Un très bon jeu de catch, ressemblant énormément à Muscle Bomber de Capcom. Sortez du rang des amateurs et prenez les 10 meilleurs espoir de lutte pour remporter le titre de champion du monde. Battez vous avec ces champions prêts à tout ! </desc><rating>0.6</rating><releasedate>19930101T000000</releasedate><developer>SNK</developer><publisher>SNK</publisher><genre>Combat</genre><players>1-2</players><hash/><image>./media/screenshots/3countb.png</image><thumbnail>./media/screenshottitles/3countb.png</thumbnail><video>./media/videos/3countb.mp4</video><genreid>262</genreid></game>";
document.LoadXml(xmlData);
Game game = new Game();
game.FromXmlDocument(document);
*/

/*
xmlData = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><gameList><provider><System>Mame</System><software>Skraper</software><database>ScreenScraper.fr</database><web>http://www.screenscraper.fr</web></provider><game id=\"13937\" source=\"ScreenScraper.fr\"><path>./3countb.zip</path><name>3 Count Bout (NGM-043 ~ NGH-043)</name><desc>Un très bon jeu de catch, ressemblant énormément à Muscle Bomber de Capcom. Sortez du rang des amateurs et prenez les 10 meilleurs espoir de lutte pour remporter le titre de champion du monde. Battez vous avec ces champions prêts à tout ! </desc><rating>0.6</rating><releasedate>19930101T000000</releasedate><developer>SNK</developer><publisher>SNK</publisher><genre>Combat</genre><players>1-2</players><hash/><image>./media/screenshots/3countb.png</image><thumbnail>./media/screenshottitles/3countb.png</thumbnail><video>./media/videos/3countb.mp4</video><genreid>262</genreid></game></gameList>";
document.LoadXml(xmlData);
*/

GameList gameList = new GameList();
var xmlFile = "C:\\Users\\olebris\\Downloads\\gamelists\\gamelist_mame.xml";
//var xmlFile = "C:\\Users\\olebris\\Downloads\\gamelists\\gamelist_cpc_ARRM.xml";
gameList.FromXmlFile(xmlFile);

Console.WriteLine("Done!");