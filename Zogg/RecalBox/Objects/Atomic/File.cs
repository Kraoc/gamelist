namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public class File
    {
        private string path = String.Empty;

        private FileInfo? fi;

        private string checksum = String.Empty;

        public File(string path = "")
        {
            this.Path = path;
        }

        public string Path
        {
            get
            {
                return this.path;
            }
            set
            {
                this.path = value;
            }
        }

        protected FileInfo Info
        {
            get
            {
                if (this.fi == null)
                {
                    this.fi = new FileInfo(this.Path);
                }

                return this.fi;
            }
        }

        public bool Exists
        {
            get
            {
                return this.Info.Exists;
            }
        }

        public FileAttributes Attributes
        {
            get
            {
                return this.Info.Attributes;
            }
        }

        public string Name
        {
            get
            {
                return this.Info.Name;
            }
        }

        public string Extension
        {
            get
            {
                return this.Info.Extension.Replace(".", "");
            }
        }

        public ExtensionTypes ExtensionType
        {
            get
            {
                switch (this.Extension.ToUpper())
                {
                    case "TXT":
                        return ExtensionTypes.TXT;

                    case "XML":
                        return ExtensionTypes.XML;

                    case "ZIP":
                        return ExtensionTypes.ZIP;

                    default:
                        return ExtensionTypes.UNKNOWN;
                }
            }
        }

        public string FullName
        {
            get
            {
                return this.Info.FullName;
            }
        }

        public string? DirectoryName
        {
            get
            {
                return this.Info.DirectoryName;
            }
        }

        public DateTime CreationTime
        {
            get
            {
                return this.Info.CreationTime;
            }
        }

        public DateTime LastAccessTime
        {
            get
            {
                return this.Info.LastAccessTime;
            }
        }

        public DateTime LastWriteTime
        {
            get
            {
                return this.Info.LastWriteTime;
            }
        }

        public long Length
        {
            get
            {
                return this.Info.Length;
            }
        }

        public string Checksum
        {
            get
            {
                if (this.checksum == String.Empty)
                {
                    this.checksum = ChecksumFile.Compute(HashingAlgoTypes.MD5, this.FullName);
                }

                return this.checksum;
            }
        }
    }

    public enum ExtensionTypes
    {
        UNKNOWN,
        TXT,
        XML,
        ZIP
    }
}
