using System;
using System.IO;
using System.Xml;

namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public class Game
    {
        private GameList? gameList = null;

        private string id = String.Empty;

        private string source = String.Empty;

        private string name = String.Empty;

        private string sortname = String.Empty;

        private string desc = String.Empty;

        private string image = String.Empty;

        private string video = String.Empty;

        private string marquee = String.Empty;

        private string thumbnail = String.Empty;

        private string rating = String.Empty;

        private string releasedate = String.Empty;

        private string developer = String.Empty;

        private string publisher = String.Empty;

        private string genre = String.Empty;

        private string players = String.Empty;

        private string favorite = String.Empty;

        private string hidden = String.Empty;

        private string kidgame = String.Empty;

        private string playcount = String.Empty;

        private string lastplayed = String.Empty;

        private string hash = String.Empty;

        private string genreid = String.Empty;

        private string path = String.Empty;

        public Game(GameList gameList)
        {
            this.GameList = gameList;
        }

        public GameList? GameList
        {
            get
            {
                return this.gameList;
            }
            set
            {
                this.gameList = value;
            }
        }

        public string Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public string Source
        {
            get
            {
                return this.source;
            }
            set
            {
                this.source = value;
            }
        }

        public string Path
        {
            get
            {
                return this.path;
            }
            set
            {
                this.path = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Sortname
        {
            get
            {
                return this.sortname;
            }
            set
            {
                this.sortname = value;
            }
        }

        public string Desc
        {
            get
            {
                return this.desc;
            }
            set
            {
                this.desc = value;
            }
        }

        public string Image
        {
            get
            {
                return this.image;
            }
            set
            {
                this.image = value;
            }
        }

        public string Video
        {
            get
            {
                return this.video;
            }
            set
            {
                this.video = value;
            }
        }

        public string Marquee
        {
            get
            {
                return this.marquee;
            }
            set
            {
                this.marquee = value;
            }
        }

        public string Thumbnail
        {
            get
            {
                return this.thumbnail;
            }
            set
            {
                this.thumbnail = value;
            }
        }

        public string Rating
        {
            get
            {
                return this.rating;
            }
            set
            {
                this.rating = value;
            }
        }

        public string Releasedate
        {
            get
            {
                return this.releasedate;
            }
            set
            {
                this.releasedate = value;
            }
        }

        public string Developer
        {
            get
            {
                return this.developer;
            }
            set
            {
                this.developer = value;
            }
        }

        public string Publisher
        {
            get
            {
                return this.publisher;
            }
            set
            {
                this.publisher = value;
            }
        }

        public string Genre
        {
            get
            {
                return this.genre;
            }
            set
            {
                this.genre = value;
            }
        }

        public string Players
        {
            get
            {
                return this.players;
            }
            set
            {
                this.players = value;
            }
        }

        public string Favorite
        {
            get
            {
                return this.favorite;
            }
            set
            {
                this.favorite = value;
            }
        }

        public string Hidden
        {
            get
            {
                return this.hidden;
            }
            set
            {
                this.hidden = value;
            }
        }

        public string Kidgame
        {
            get
            {
                return this.kidgame;
            }
            set
            {
                this.kidgame = value;
            }
        }

        public string Playcount
        {
            get
            {
                return this.playcount;
            }
            set
            {
                this.playcount = value;
            }
        }

        public string Lastplayed
        {
            get
            {
                return this.lastplayed;
            }
            set
            {
                this.lastplayed = value;
            }
        }

        public string Hash
        {
            get
            {
                return this.hash;
            }
            set
            {
                this.hash = value;
            }
        }

        public string GenreId
        {
            get
            {
                return this.genreid;
            }
            set
            {
                this.genreid = value;
            }
        }

        public string GameFile
        {
            get
            {
                return System.IO.Path.GetFileName(this.Path);
            }
        }

        public string GameFileName
        {
            get
            {
                return System.IO.Path.GetFileNameWithoutExtension(this.GameFile);
            }
        }

        public string GameFileExtension
        {
            get
            {
                return System.IO.Path.GetExtension(this.GameFile).Replace(".", "");
            }
        }

        public string GamePath
        {
            get
            {
                if (this.GameList != null && this.GameList.GameListFile.DirectoryName != null)
                {
                    return System.IO.Path.Combine(this.GameList.GameListFile.DirectoryName, this.GameFile);
                }
                else
                {
                    return this.Path;
                }
            }
        }

        public DateTime GameReleaseDate
        {
            get
            {
                if (this.Releasedate == String.Empty)
                {
                    return new DateTime();
                }
                else
                {
                    string rd = this.Releasedate;
                    string YYYY = rd.Substring(0, 4);
                    string MM = rd.Substring(4, 2);
                    string DD = rd.Substring(6, 2);
                    string hh = rd.Substring(9, 2);
                    string mm = rd.Substring(11, 2);
                    string ss = rd.Substring(13, 2);
                    return new DateTime(Int32.Parse(YYYY), Int32.Parse(MM), Int32.Parse(DD), Int32.Parse(hh), Int32.Parse(mm), Int32.Parse(ss));
                }
            }
        }

        public void FromXmlDocument(XmlDocument document)
        {
            if (document != null && document.FirstChild != null)
            {
                XmlNode? node = document.FirstChild;
                XmlAttributeCollection? attributes = node.Attributes;

                if (attributes != null)
                {
                    if (attributes["id"] != null)
                    {
                        this.Id = attributes["id"]!.InnerText;
                    }

                    if (attributes["source"] != null)
                    {
                        this.Source = attributes["source"]!.InnerText;
                    }
                }

                if (node != null)
                {
                    if (node["path"] != null)
                    {
                        this.Path = node["path"]!.InnerText;
                    }

                    if (node["name"] != null)
                    {
                        this.Name = node["name"]!.InnerText;
                    }

                    if (node["desc"] != null)
                    {
                        this.Desc = node["desc"]!.InnerText;
                    }

                    if (node["rating"] != null)
                    {
                        this.Rating = node["rating"]!.InnerText;
                    }

                    if (node["releasedate"] != null)
                    {
                        this.Releasedate = node["releasedate"]!.InnerText;
                    }

                    if (node["developer"] != null)
                    {
                        this.Developer = node["developer"]!.InnerText;
                    }

                    if (node["publisher"] != null)
                    {
                        this.Publisher = node["publisher"]!.InnerText;
                    }

                    if (node["genre"] != null)
                    {
                        this.Genre = node["genre"]!.InnerText;
                    }

                    if (node["players"] != null)
                    {
                        this.Players = node["players"]!.InnerText;
                    }

                    if (node["hash"] != null)
                    {
                        this.Hash = node["hash"]!.InnerText;
                    }

                    if (node["image"] != null)
                    {
                        this.Image = node["image"]!.InnerText;
                    }

                    if (node["thumbnail"] != null)
                    {
                        this.Thumbnail = node["thumbnail"]!.InnerText;
                    }

                    if (node["video"] != null)
                    {
                        this.Video = node["video"]!.InnerText;
                    }

                    if (node["genreid"] != null)
                    {
                        this.GenreId = node["genreid"]!.InnerText;
                    }

                    if (node["sortname"] != null)
                    {
                        this.Sortname = node["sortname"]!.InnerText;
                    }

                    if (node["marquee"] != null)
                    {
                        this.Marquee = node["marquee"]!.InnerText;
                    }

                    if (node["favorite"] != null)
                    {
                        this.Favorite = node["favorite"]!.InnerText;
                    }

                    if (node["hidden"] != null)
                    {
                        this.Hidden = node["hidden"]!.InnerText;
                    }

                    if (node["kidgame"] != null)
                    {
                        this.Kidgame = node["kidgame"]!.InnerText;
                    }

                    if (node["playcount"] != null)
                    {
                        this.Playcount = node["playcount"]!.InnerText;
                    }

                    if (node["lastplayed"] != null)
                    {
                        this.Lastplayed = node["lastplayed"]!.InnerText;
                    }
                }
            }
        }

        override public string ToString()
        {
            return this.GameFileName;
        }
    }
}