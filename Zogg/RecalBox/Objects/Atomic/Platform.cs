namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public class Platform
    {
        private int index;

        private string? id;

        private string? name;

        public Platform(int index = 0, string id = "", string name = "")
        {
            this.Index = index;
            this.Id = id;
            this.Name = name;
        }

        public int Index
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
            }
        }

        public string? Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public string? Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
    }
}