using System;
using System.Xml;

namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public class Folder
    {
        private string id = String.Empty;

        private string source = String.Empty;

        private string name = String.Empty;

        private string sortname = String.Empty;

        private string desc = String.Empty;

        private string image = String.Empty;

        private string thumbnail = String.Empty;

        private string video = String.Empty;

        private string marquee = String.Empty;

        private string rating = String.Empty;

        private string releasedate = String.Empty;

        private string developer = String.Empty;

        private string publisher = String.Empty;

        private string genre = String.Empty;

        private string players = String.Empty;

        public string Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public string Source
        {
            get
            {
                return this.source;
            }
            set
            {
                this.source = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Sortname
        {
            get
            {
                return this.sortname;
            }
            set
            {
                this.sortname = value;
            }
        }

        public string Desc
        {
            get
            {
                return this.desc;
            }
            set
            {
                this.desc = value;
            }
        }

        public string Image
        {
            get
            {
                return this.image;
            }
            set
            {
                this.image = value;
            }
        }

        public string Thumbnail
        {
            get
            {
                return this.thumbnail;
            }
            set
            {
                this.thumbnail = value;
            }
        }

        public string Video
        {
            get
            {
                return this.video;
            }
            set
            {
                this.video = value;
            }
        }

        public string Marquee
        {
            get
            {
                return this.marquee;
            }
            set
            {
                this.marquee = value;
            }
        }

        public string Rating
        {
            get
            {
                return this.rating;
            }
            set
            {
                this.rating = value;
            }
        }

        public string Releasedate
        {
            get
            {
                return this.releasedate;
            }
            set
            {
                this.releasedate = value;
            }
        }

        public string Developer
        {
            get
            {
                return this.developer;
            }
            set
            {
                this.developer = value;
            }
        }

        public string Publisher
        {
            get
            {
                return this.publisher;
            }
            set
            {
                this.publisher = value;
            }
        }

        public string Genre
        {
            get
            {
                return this.genre;
            }
            set
            {
                this.genre = value;
            }
        }

        public string Players
        {
            get
            {
                return this.players;
            }
            set
            {
                this.players = value;
            }
        }

        public void FromXmlDocument(XmlDocument document)
        {
            if (document != null && document.FirstChild != null)
            {
                XmlNode? node = document.FirstChild;
                XmlAttributeCollection? attributes = node.Attributes;

                if (attributes != null)
                {
                    if (attributes["id"] != null)
                    {
                        this.Id = attributes["id"]!.InnerText;
                    }

                    if (attributes["source"] != null)
                    {
                        this.Source = attributes["source"]!.InnerText;
                    }
                }

                if (node != null)
                {
                    if (node["name"] != null)
                    {
                        this.Name = node["name"]!.InnerText;
                    }

                    if (node["sortname"] != null)
                    {
                        this.Sortname = node["sortname"]!.InnerText;
                    }

                    if (node["desc"] != null)
                    {
                        this.Desc = node["desc"]!.InnerText;
                    }

                    if (node["rating"] != null)
                    {
                        this.Rating = node["rating"]!.InnerText;
                    }

                    if (node["releasedate"] != null)
                    {
                        this.Releasedate = node["releasedate"]!.InnerText;
                    }

                    if (node["developer"] != null)
                    {
                        this.Developer = node["developer"]!.InnerText;
                    }

                    if (node["publisher"] != null)
                    {
                        this.Publisher = node["publisher"]!.InnerText;
                    }

                    if (node["genre"] != null)
                    {
                        this.Genre = node["genre"]!.InnerText;
                    }

                    if (node["players"] != null)
                    {
                        this.Players = node["players"]!.InnerText;
                    }

                    if (node["image"] != null)
                    {
                        this.Image = node["image"]!.InnerText;
                    }

                    if (node["thumbnail"] != null)
                    {
                        this.Thumbnail = node["thumbnail"]!.InnerText;
                    }

                    if (node["video"] != null)
                    {
                        this.Video = node["video"]!.InnerText;
                    }

                    if (node["genre"] != null)
                    {
                        this.Genre = node["genre"]!.InnerText;
                    }

                    if (node["marquee"] != null)
                    {
                        this.Marquee = node["marquee"]!.InnerText;
                    }
                }
            }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}