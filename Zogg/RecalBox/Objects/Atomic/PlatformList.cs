namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public class PlatformList
    {
        private List<Platform> platforms = new List<Platform>();

        public List<Platform> Platforms
        {
            get
            {
                return this.platforms;
            }
            set
            {
                this.platforms = value;
            }
        }

        public PlatformList()
        {
            this.Platforms.Add(new Platform(0, "PLATFORM_UNKNOWN", "unknown"));
            this.Platforms.Add(new Platform(1, "THREEDO", "3do"));
            this.Platforms.Add(new Platform(2, "AMIGA", "amiga"));
            this.Platforms.Add(new Platform(3, "AMSTRAD_CPC", "amstradcpc"));
            this.Platforms.Add(new Platform(4, "APPLE_II", "apple2"));
            this.Platforms.Add(new Platform(5, "ARCADE", "arcade"));
            this.Platforms.Add(new Platform(6, "ATARI_800", "atari800"));
            this.Platforms.Add(new Platform(7, "ATARI_2600", "atari2600"));
            this.Platforms.Add(new Platform(8, "ATARI_5200", "atari5200"));
            this.Platforms.Add(new Platform(9, "ATARI_7800", "atari7800"));
            this.Platforms.Add(new Platform(10, "ATARI_LYNX", "atarilynx"));
            this.Platforms.Add(new Platform(11, "ATARI_ST", "atarist"));
            this.Platforms.Add(new Platform(12, "ATARI_JAGUAR", "atarijaguar"));
            this.Platforms.Add(new Platform(13, "ATARI_JAGUAR_CD", "atarijaguarcd"));
            this.Platforms.Add(new Platform(14, "ATARI_XE", "atarixe"));
            this.Platforms.Add(new Platform(15, "BBC_MICRO", "bbcmicro"));
            this.Platforms.Add(new Platform(16, "COLECOVISION", "colecovision"));
            this.Platforms.Add(new Platform(17, "COMMODORE_64", "c64"));
            this.Platforms.Add(new Platform(18, "DAPHNE", "daphne"));
            this.Platforms.Add(new Platform(19, "INTELLIVISION", "intellivision"));
            this.Platforms.Add(new Platform(20, "MAC_OS", "macintosh"));
            this.Platforms.Add(new Platform(21, "XBOX", "xbox"));
            this.Platforms.Add(new Platform(22, "XBOX_360", "xbox360"));
            this.Platforms.Add(new Platform(23, "MSX", "msx"));
            this.Platforms.Add(new Platform(24, "NEOGEO", "neogeo"));
            this.Platforms.Add(new Platform(25, "NEOGEO_CD", "neogeocd"));
            this.Platforms.Add(new Platform(26, "NEOGEO_POCKET", "ngp"));
            this.Platforms.Add(new Platform(27, "NEOGEO_POCKET_COLOR", "ngpc"));
            this.Platforms.Add(new Platform(28, "NINTENDO_3DS", "n3ds"));
            this.Platforms.Add(new Platform(29, "NINTENDO_64", "n64"));
            this.Platforms.Add(new Platform(30, "NINTENDO_DS", "nds"));
            this.Platforms.Add(new Platform(31, "FAMICOM_DISK_SYSTEM", "fds"));
            this.Platforms.Add(new Platform(32, "NINTENDO_ENTERTAINMENT_SYSTEM", "nes"));
            this.Platforms.Add(new Platform(33, "NINTENDO_POKEMON_MINI", "pokemini"));
            this.Platforms.Add(new Platform(34, "FAIRCHILD_CHANNELF", "channelf"));
            this.Platforms.Add(new Platform(35, "GAME_BOY", "gb"));
            this.Platforms.Add(new Platform(36, "GAME_BOY_ADVANCE", "gba"));
            this.Platforms.Add(new Platform(37, "GAME_BOY_COLOR", "gbc"));
            this.Platforms.Add(new Platform(38, "NINTENDO_GAMECUBE", "gc"));
            this.Platforms.Add(new Platform(39, "NINTENDO_WII", "wii"));
            this.Platforms.Add(new Platform(40, "NINTENDO_WII_U", "wiiu"));
            this.Platforms.Add(new Platform(41, "NINTENDO_VIRTUAL_BOY", "virtualboy"));
            this.Platforms.Add(new Platform(42, "NINTENDO_GAME_AND_WATCH", "gameandwatch"));
            this.Platforms.Add(new Platform(43, "NINTENDO_SWITCH", "switch"));
            this.Platforms.Add(new Platform(44, "OPENBOR", "openbor"));
            this.Platforms.Add(new Platform(45, "PC", "pc"));
            this.Platforms.Add(new Platform(46, "SEGA_32X", "sega32x"));
            this.Platforms.Add(new Platform(47, "SEGA_CD", "segacd"));
            this.Platforms.Add(new Platform(48, "SEGA_DREAMCAST", "dreamcast"));
            this.Platforms.Add(new Platform(49, "SEGA_GAME_GEAR", "gamegear"));
            this.Platforms.Add(new Platform(50, "SEGA_GENESIS", "genesis"));
            this.Platforms.Add(new Platform(51, "SEGA_MASTER_SYSTEM", "mastersystem"));
            this.Platforms.Add(new Platform(52, "SEGA_MEGA_DRIVE", "megadrive"));
            this.Platforms.Add(new Platform(53, "SEGA_SATURN", "saturn"));
            this.Platforms.Add(new Platform(54, "SEGA_SG1000", "sg-1000"));
            this.Platforms.Add(new Platform(55, "SAM_COUPE", "samcoupe"));
            this.Platforms.Add(new Platform(56, "PLAYSTATION", "psx"));
            this.Platforms.Add(new Platform(57, "PLAYSTATION_2", "ps2"));
            this.Platforms.Add(new Platform(58, "PLAYSTATION_3", "ps3"));
            this.Platforms.Add(new Platform(59, "PLAYSTATION_4", "ps4"));
            this.Platforms.Add(new Platform(60, "PLAYSTATION_VITA", "psvita"));
            this.Platforms.Add(new Platform(61, "PLAYSTATION_PORTABLE", "psp"));
            this.Platforms.Add(new Platform(62, "SUPER_NINTENDO", "snes"));
            this.Platforms.Add(new Platform(63, "SCUMMVM", "scummvm"));
            this.Platforms.Add(new Platform(64, "SHARP_X1", "x1"));
            this.Platforms.Add(new Platform(65, "SHARP_X6800", "x68000"));
            this.Platforms.Add(new Platform(66, "SOLARUS", "solarus"));
            this.Platforms.Add(new Platform(67, "PICO_8", "pico8"));
            this.Platforms.Add(new Platform(68, "TIC_80", "tic80"));
            this.Platforms.Add(new Platform(69, "THOMSON_MOTO", "moto"));
            this.Platforms.Add(new Platform(70, "NEC_PC_8801", "pc88"));
            this.Platforms.Add(new Platform(71, "NEC_PC_9801", "pc98"));
            this.Platforms.Add(new Platform(72, "TURBOGRAFX_16", "pcengine"));
            this.Platforms.Add(new Platform(73, "TURBOGRAFX_CD", "pcenginecd"));
            this.Platforms.Add(new Platform(74, "NEC_PCFX", "pcfx"));
            this.Platforms.Add(new Platform(75, "WONDERSWAN", "wonderswan"));
            this.Platforms.Add(new Platform(76, "WONDERSWAN_COLOR", "wonderswancolor"));
            this.Platforms.Add(new Platform(77, "ZX_SPECTRUM", "zxspectrum"));
            this.Platforms.Add(new Platform(78, "ZX81_SINCLAR", "zx81"));
            this.Platforms.Add(new Platform(79, "VIDEOPAC_ODYSSEY2", "videopac"));
            this.Platforms.Add(new Platform(80, "VECTREX", "vectrex"));
            this.Platforms.Add(new Platform(81, "TRS80_COLOR_COMPUTER", "trs-80"));
            this.Platforms.Add(new Platform(82, "TANDY", "coco"));
            this.Platforms.Add(new Platform(83, "TI_99", "ti99"));
            this.Platforms.Add(new Platform(84, "DRAGON32", "dragon32"));
            this.Platforms.Add(new Platform(85, "ZMACHINE", "zmachine"));
            this.Platforms.Add(new Platform(86, "PLATFORM_IGNORE", "ignore"));
            this.Platforms.Add(new Platform(87, "PLATFORM_COUNT", "invalid"));
        }
    }
}