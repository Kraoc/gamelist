using Nito.HashAlgorithms;

namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public static class ChecksumFile
    {
        public static string Compute(HashingAlgoTypes hashingAlgoType, string filename)
        {
            if (hashingAlgoType == HashingAlgoTypes.CRC32)
            {
                using (var hasher = new CRC32())
                {
                    using (var stream = System.IO.File.OpenRead(filename))
                    {
                        if (hasher != null)
                        {
                            var hash = hasher.ComputeHash(stream);
                            return BitConverter.ToString(hash).Replace("-", "");
                        }
                        else
                        {
                            return String.Empty;
                        }
                    }
                }
            }
            else
            {
                using (var hasher = System.Security.Cryptography.HashAlgorithm.Create(hashingAlgoType.ToString()))
                {
                    using (var stream = System.IO.File.OpenRead(filename))
                    {
                        if (hasher != null)
                        {
                            var hash = hasher.ComputeHash(stream);
                            return BitConverter.ToString(hash).Replace("-", "");
                        }
                        else
                        {
                            return String.Empty;
                        }
                    }
                }
            }
        }
    }

    public enum HashingAlgoTypes
    {
        CRC32,

        MD5,

        SHA1,

        SHA256,

        SHA384,

        SHA512
    }
}
