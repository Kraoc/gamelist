using System;
using System.Xml;

namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public class GameListGroup
    {
        public string? Id;

        public List<Game>? Games;

        public override string ToString()
        {
            return this.Id!;
        }
    }

    public class GenreListGroup
    {
        public string? Id;

        public List<string>? Genre;

        public override string ToString()
        {
            return this.Id!;
        }
    }

    public class GameList
    {
        private File xmlFile = new File();

        private Provider provider = new Provider();

        private List<Game> games = new List<Game>();

        private List<Folder> folders = new List<Folder>();

        private List<GameListGroup>? gamesByDeveloppers = null;

        private List<GameListGroup>? gamesByPublishers = null;

        private List<GameListGroup>? gamesByGenres = null;

        private List<GameListGroup>? gamesByGenreIds = null;

        private List<GenreListGroup>? genreByGenreIds = null;

        private List<GenreListGroup>? genreIdByGenres = null;

        private List<string>? developpers = null;

        private List<string>? publishers = null;

        private List<string>? genres = null;

        private List<string>? genreids = null;

        private List<string>? extensions = null;

        private Int32 count = 0;

        public File GameListFile
        {
            get
            {
                return this.xmlFile;
            }
            set
            {
                this.xmlFile = value;
            }
        }

        public Provider Provider
        {
            get
            {
                return this.provider;
            }
            set
            {
                this.provider = value;
            }
        }

        public List<Game> Games
        {
            get
            {
                return this.games;
            }
            set
            {
                this.games = value;
            }
        }

        public Int32 Count
        {
            get
            {
                if (this.count == 0)
                {
                    this.count = this.Games.Count;
                }

                return this.count;
            }
        }

        public List<Folder> Folders
        {
            get
            {
                return this.folders;
            }
            set
            {
                this.folders = value;
            }
        }

        public List<string> Developpers
        {
            get
            {
                if (this.developpers == null)
                {
                    List<string> list = new List<string>();

                    foreach (Game g in this.Games)
                    {
                        if (g.Developer != String.Empty)
                        {
                            if (!list.Contains(g.Developer))
                            {
                                list.Add(g.Developer);
                            }
                        }
                    }

                    list.Sort();

                    this.developpers = list;
                }

                return this.developpers;
            }
        }

        public List<string> Publishers
        {
            get
            {
                if (this.publishers == null)
                {
                    List<string> list = new List<string>();

                    foreach (Game g in this.Games)
                    {
                        if (g.Publisher != String.Empty)
                        {
                            if (!list.Contains(g.Publisher))
                            {
                                list.Add(g.Publisher);
                            }
                        }
                    }

                    list.Sort();

                    this.publishers = list;
                }

                return this.publishers;
            }
        }

        public List<string> Genres
        {
            get
            {
                if (this.genres == null)
                {
                    List<string> list = new List<string>();

                    foreach (Game g in this.Games)
                    {
                        if (g.Genre != String.Empty)
                        {
                            if (!list.Contains(g.Genre))
                            {
                                list.Add(g.Genre);
                            }
                        }
                    }

                    list.Sort();

                    this.genres = list;
                }

                return this.genres;
            }
        }

        public List<string> GenreIds
        {
            get
            {
                if (this.genreids == null)
                {
                    List<string> list = new List<string>();

                    foreach (Game g in this.Games)
                    {
                        if (g.GenreId != String.Empty)
                        {
                            if (!list.Contains(g.GenreId))
                            {
                                list.Add(g.GenreId);
                            }
                        }
                    }

                    list.Sort();

                    this.genreids = list;
                }

                return this.genreids;
            }
        }

        public List<string> Extensions
        {
            get
            {
                if (this.extensions == null)
                {
                    List<string> list = new List<string>();

                    foreach (Game g in this.Games)
                    {
                        if (g.GameFileExtension != String.Empty)
                        {
                            if (!list.Contains(g.GameFileExtension))
                            {
                                list.Add(g.GameFileExtension);
                            }
                        }
                    }

                    list.Sort();

                    this.extensions = list;
                }

                return this.extensions;
            }
        }

        private List<Game> GamesByDevelopper(string developper)
        {
            List<Game> list = new List<Game>();

            foreach (Game g in this.Games)
            {
                if (g.Developer != String.Empty)
                {
                    if (!list.Contains(g) && g.Developer == developper)
                    {
                        list.Add(g);
                    }
                }
            }

            return list;
        }

        private List<Game> GamesByPublisher(string publisher)
        {
            List<Game> list = new List<Game>();

            foreach (Game g in this.Games)
            {
                if (g.Publisher != String.Empty)
                {
                    if (!list.Contains(g) && g.Publisher == publisher)
                    {
                        list.Add(g);
                    }
                }
            }

            return list;
        }

        private List<Game> GamesByGenre(string genre)
        {
            List<Game> list = new List<Game>();

            foreach (Game g in this.Games)
            {
                if (g.Genre != String.Empty)
                {
                    if (!list.Contains(g) && g.Genre == genre)
                    {
                        list.Add(g);
                    }
                }
            }

            return list;
        }

        private List<Game> GamesByGenreId(string genreId)
        {
            List<Game> list = new List<Game>();

            foreach (Game g in this.Games)
            {
                if (g.GenreId != String.Empty)
                {
                    if (!list.Contains(g) && g.GenreId == genreId)
                    {
                        list.Add(g);
                    }
                }
            }

            return list;
        }

        private List<string> GenresByGenreId(string genreId)
        {
            List<string> list = new List<string>();

            foreach (Game g in this.Games)
            {
                if (g.GenreId != String.Empty)
                {
                    if (g.GenreId == genreId && !list.Contains(g.Genre))
                    {
                        list.Add(g.Genre);
                    }
                }
            }

            return list;
        }

        private List<string> GenreIdsByGenre(string genre)
        {
            List<string> list = new List<string>();

            foreach (Game g in this.Games)
            {
                if (g.Genre != String.Empty)
                {
                    if (g.Genre == genre && !list.Contains(g.GenreId))
                    {
                        list.Add(g.GenreId);
                    }
                }
            }

            return list;
        }

        public List<GameListGroup> GamesByDeveloppers
        {
            get
            {
                if (this.gamesByDeveloppers == null)
                {
                    List<GameListGroup> list = new List<GameListGroup>();

                    foreach (string developper in this.Developpers)
                    {
                        GameListGroup group = new GameListGroup();

                        group.Id = developper;
                        group.Games = this.GamesByDevelopper(developper);

                        list.Add(group);
                    }

                    this.gamesByDeveloppers = list;
                }

                return this.gamesByDeveloppers;
            }
        }

        public List<GameListGroup> GamesByPublishers
        {
            get
            {
                if (this.gamesByPublishers == null)
                {
                    List<GameListGroup> list = new List<GameListGroup>();

                    foreach (string publisher in this.Publishers)
                    {
                        GameListGroup group = new GameListGroup();

                        group.Id = publisher;
                        group.Games = this.GamesByPublisher(publisher);

                        list.Add(group);
                    }

                    this.gamesByPublishers = list;
                }

                return this.gamesByPublishers;
            }
        }

        public List<GameListGroup> GamesByGenres
        {
            get
            {
                if (this.gamesByGenres == null)
                {
                    List<GameListGroup> list = new List<GameListGroup>();

                    foreach (string genre in this.Genres)
                    {
                        GameListGroup group = new GameListGroup();

                        group.Id = genre;
                        group.Games = this.GamesByGenre(genre);

                        list.Add(group);
                    }

                    this.gamesByGenres = list;
                }

                return this.gamesByGenres;
            }
        }

        public List<GameListGroup> GamesByGenreIds
        {
            get
            {
                if (this.gamesByGenreIds == null)
                {
                    List<GameListGroup> list = new List<GameListGroup>();

                    foreach (string genreId in this.GenreIds)
                    {
                        GameListGroup group = new GameListGroup();

                        group.Id = genreId;
                        group.Games = this.GamesByGenreId(genreId);

                        list.Add(group);
                    }

                    this.gamesByGenreIds = list;
                }

                return this.gamesByGenreIds;
            }
        }

        public List<GenreListGroup> GenreByGenreIds
        {
            get
            {
                if (this.genreByGenreIds == null)
                {
                    List<GenreListGroup> list = new List<GenreListGroup>();

                    foreach (string genreId in this.GenreIds)
                    {
                        GenreListGroup group = new GenreListGroup();

                        group.Id = genreId;
                        group.Genre = this.GenresByGenreId(genreId);

                        list.Add(group);
                    }

                    this.genreByGenreIds = list;
                }

                return this.genreByGenreIds;
            }
        }

        public List<GenreListGroup> GenreIdByGenres
        {
            get
            {
                if (this.genreIdByGenres == null)
                {
                    List<GenreListGroup> list = new List<GenreListGroup>();

                    foreach (string genre in this.Genres)
                    {
                        GenreListGroup group = new GenreListGroup();

                        group.Id = genre;
                        group.Genre = this.GenreIdsByGenre(genre);

                        list.Add(group);
                    }

                    this.genreIdByGenres = list;
                }

                return this.genreIdByGenres;
            }
        }

        public void FromXmlDocument(XmlDocument document)
        {
            XmlElement? root = document.DocumentElement;

            if (root != null && root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    XmlNode? node = root.ChildNodes[i];

                    if (node != null)
                    {
                        if (node.Name == "provider")
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(node.OuterXml);

                            this.Provider.FromXmlDocument(doc);
                        }

                        if (node.Name == "game")
                        {
                            Game game = new Game(this);

                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(node.OuterXml);

                            game.FromXmlDocument(doc);

                            this.Games.Add(game);
                        }

                        if (node.Name == "folder")
                        {
                            Folder folder = new Folder();

                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(node.OuterXml);

                            folder.FromXmlDocument(doc);

                            this.Folders.Add(folder);
                        }
                    }
                }
            }
        }

        public void Reload()
        {
            using (FileStream filStream = new FileStream(this.GameListFile.FullName, FileMode.Open))
            {
                using (BufferedStream bufStream = new BufferedStream(filStream))
                {
                    bufStream.Seek(0, SeekOrigin.Begin);

                    XmlDocument document = new XmlDocument();
                    document.Load(bufStream);

                    this.FromXmlDocument(document);
                }
            }
        }

        public void FromXmlFile(string path)
        {
            this.GameListFile.Path = path;

            this.Reload();
        }
    }
}