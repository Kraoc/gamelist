using System;
using System.Xml;

namespace Zogg.RecalBox.GameList.Objects.Atomic
{
    public class Provider
    {
        private string system = String.Empty;

        private string software = String.Empty;

        private string database = String.Empty;

        private string web = String.Empty;

        public string System
        {
            get
            {
                return this.system;
            }
            set
            {
                this.system = value;
            }
        }

        public string Software
        {
            get
            {
                return this.software;
            }
            set
            {
                this.software = value;
            }
        }

        public string Database
        {
            get
            {
                return this.database;
            }
            set
            {
                this.database = value;
            }
        }

        public string Web
        {
            get
            {
                return this.web;
            }
            set
            {
                this.web = value;
            }
        }

        public void FromXmlDocument(XmlDocument document)
        {
            XmlNode? node = document.FirstChild;

            if (node != null)
            {
                if (node["System"] != null)
                {
                    this.System = node["System"]!.InnerText;
                }

                if (node["software"] != null)
                {
                    this.Software = node["software"]!.InnerText;
                }

                if (node["database"] != null)
                {
                    this.Database = node["database"]!.InnerText;
                }

                if (node["web"] != null)
                {
                    this.Web = node["web"]!.InnerText;
                }
            }
        }
    }
}
